package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;


import play.*;
import play.mvc.*;

import views.html.*;


public class Application extends Controller {

   public static Result Index() {
	return (redirect("/views/index.html"));
   }
	
   public static Result readFile(String fileName) throws IOException{
		
	   byte[] encoded = Files.readAllBytes(Paths.get("app//DATA//" + fileName + ".json"));
	   
	   String str = new String(encoded);
	   return ok(str);
	 }

}
