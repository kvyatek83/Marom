var app = angular.module('Marom', ['ngRoute']);


app.controller('customersCtrl', function ($scope) {


});


app.service('fileHandler', function ($http, $q) {

    this.getObjects = function (path) {
        var result = $q.defer();
        $http.get(path)
            .then(function (data) {
                result.resolve(data);
            }, function (data) {
                result.reject(data);
            });
        return result.promise;
    };

});







app.directive('theNavbar', function () {
    return {
        restrict: 'EACM',
        templateUrl: "../views/directives/Navbar.html",
        replace: false
    };
});

app.directive('objectsTable', function () {
    return {
        restrict: 'EACM',
        templateUrl: "../views/directives/objectsTable.html",
        replace: false
    };
});






app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/', {
            templateUrl: '../views/Route/Home.html'
        })
        .when('/perfumes', {
            templateUrl: '../views/Route/perfumes.html'
        })
        .when('/makeups', {
            templateUrl: '../views/Route/makeups.html'
        })
        .otherwise({
            redirectTo: '/'
        });;

}]);
